package com.iban.emailservice.jms;

import com.iban.emailservice.mailing.EmailService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JmsConsumer {

    private JmsTemplate jmsTemplateTopic;

    private EmailService emailService;

    @JmsListener (destination = "${spring.activemq.topic}", containerFactory = "jmsFactoryTopic")
    public void onReceiverTopic(String str) {
        JSONObject obj = new JSONObject(str);
        emailService.sendSimpleMessage(obj.getString("email"), "testing", "Hello World! " );
    }

    public JmsConsumer (JmsTemplate jmsTemplateTopic, EmailService emailService){
        this.jmsTemplateTopic = jmsTemplateTopic;
        this.emailService = emailService;
    }

}
