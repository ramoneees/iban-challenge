# iban-challenge



## To run this challenge

1 - run ./build-images.sh to create docker images for subscription and email services

2 - run docker-compose up. It will load up all dependencies such as database, smtp server and activemq, and also load subscritpion and email services

3 - You can use the postman on this repo to test, and go to localhost:8025 to check if the email was sended to the server

# Dependencies that i used

[Mailhog](https://github.com/mailhog/MailHog) for SMTP server

[ActiveMQ](https://activemq.apache.org/) for communication between services.

MySQL for database server



# How i did



subscription-service exposes a POST endpoint /subscriptions, when you pass this json, it will persist into mysql database. When data is persisted, it sends a message to activemq topic, which email-service is subscribed. When topic is collected, it got the email and send the email to the smtp server (Pointing to mailhog)