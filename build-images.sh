#!/bin/bash

clear

cd subscription-service

./gradlew bootBuildImage

cd ../email-service

./gradlew bootBuildImage