package com.iban.subscriptionservice.controller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;

import com.iban.subscriptionservice.model.Subscription;
import com.iban.subscriptionservice.service.SubscriptionService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@WebMvcTest(value = SubscriptionController.class, excludeAutoConfiguration = SecurityAutoConfiguration.class)
public class SubscriptionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SubscriptionService service;


    @Test
    public void whenPostSubscription_thenCreateSubscription() throws Exception {
        Subscription s = new Subscription();
        s.setFirstName("Ramon Rios");
        s.setConsent(true);
        s.setDateOfBirth("06/07/1993");
        s.setEmail("email@email.com");
        s.setNewsletterId("1");
        given(service.save(Mockito.any())).willReturn(s);
        mvc.perform(post("/subscription").contentType(MediaType.APPLICATION_JSON).content(JSONUtils.toJson(s))).andExpect(status().isCreated());
        verify(service, VerificationModeFactory.times(1)).save(Mockito.any());
        reset(service);
    }


    @Test
    public void givenSubscriptionId_whenGetSubscription_thenReturnJson() throws Exception {
        Subscription s = new Subscription();
        s.setId(1);
        s.setFirstName("Ramon Rios");
        s.setConsent(true);
        s.setDateOfBirth("06/07/1993");
        s.setEmail("email@email.com");
        s.setNewsletterId("1");
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("id", "1");
        given(service.getSubscriptionById(Mockito.anyInt())).willReturn(s);

        mvc.perform(get("/subscription/" + "1").contentType(MediaType.APPLICATION_JSON).params(params)).andExpect(jsonPath("$.firstName", is("Ramon Rios")));
        verify(service, VerificationModeFactory.times(1)).getSubscriptionById(Mockito.anyInt());
        reset(service);
    }


}
