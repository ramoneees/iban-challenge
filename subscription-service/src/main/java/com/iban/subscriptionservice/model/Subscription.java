package com.iban.subscriptionservice.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Entity
@Table
@Data
@AllArgsConstructor
public class Subscription implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String email;

    @Column
    private String firstName;

    @Column
    private String gender;

    @Column(nullable = false)
    @NonNull
    private String dateOfBirth;

    @Column(nullable = false)
    private boolean consent;

    @Column(nullable = false)
    private String newsletterId;

    public Subscription() {

    }
}
