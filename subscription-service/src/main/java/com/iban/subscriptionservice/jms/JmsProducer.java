package com.iban.subscriptionservice.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JmsProducer {

    @Autowired
    private JmsTemplate jmsTemplateTopic;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${spring.activemq.topic}")
    private String topic;

    public void sendMessage(Object obj){
        try {
            String objAsJson = objectMapper.writeValueAsString(obj);
            System.out.println(objAsJson);

            jmsTemplateTopic.convertAndSend(topic,objAsJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
