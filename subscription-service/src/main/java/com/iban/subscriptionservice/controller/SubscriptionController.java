package com.iban.subscriptionservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iban.subscriptionservice.exceptions.SubscriptionNotFoundException;
import com.iban.subscriptionservice.jms.JmsProducer;
import com.iban.subscriptionservice.model.Subscription;
import com.iban.subscriptionservice.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import java.net.URI;
import java.util.Optional;


@RestController
public class SubscriptionController {
    @Autowired
    private SubscriptionService service;

    @Autowired
    private JmsProducer jmsProducer;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/subscriptions")
    public ResponseEntity<Object> createSubscription(@RequestBody Subscription subscription){

        Subscription savedSubscription = service.save(subscription);
        jmsProducer.sendMessage(savedSubscription);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedSubscription.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/subscriptions/{id}")
    public Subscription getSubscriptionById(@PathVariable("id") int subscriptionId){
        Subscription s = service.getSubscriptionById(subscriptionId);
        Optional<Subscription> subscriptionOptional = Optional.ofNullable(s);
        if (!subscriptionOptional.isPresent())
            throw new SubscriptionNotFoundException("Subscription not found");

        return s;
    }
}
