package com.iban.subscriptionservice.exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)

public class SubscriptionAlreadyExistException extends RuntimeException {

  public SubscriptionAlreadyExistException(String exception) {
		  super(exception);
	}
}